class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hour = seconds / 3600
    min = (seconds - 3600 * hour) / 60
    sec = seconds - (3600 * hour) - (60 * min)

    "#{num_to_time(hour)}:#{num_to_time(min)}:#{num_to_time(sec)}"
  end

  def num_to_time(num)
    num.to_s.length == 1 ? "0#{num}" : num.to_s
  end

end
