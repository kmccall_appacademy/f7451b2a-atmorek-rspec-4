class Dictionary
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(entry)
    if entry.is_a?(Hash)
      entries[entry.keys.first] = entry.values.first
    else
      entries[entry] = nil
    end
  end

  def keywords
    entries.keys.sort
  end

  def include?(entry)
    keywords.include?(entry)
  end

  def find(prefix)
    entries.select { |k, _v| k[0, prefix.length] == prefix }
  end

  def printable
    output = []
    keywords.each { |k| output << %Q{[#{k}] "#{entries[k]}"} }
    output.join("\n")
  end

end

if __FILE__ == $PROGRAM_NAME
  d = Dictionary.new
  d.add("zebra" => "African land animal with stripes")
  d.add("fish" => "aquatic animal")
  d.add("apple" => "fruit")
  puts d.printable
end
