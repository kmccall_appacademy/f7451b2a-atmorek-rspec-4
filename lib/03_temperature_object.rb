class Temperature
  attr_reader :temp, :type

  def initialize(temp_hash)
    @type, @temp = temp_hash.to_a.first
  end

  def in_fahrenheit
    type == :f ? temp : Temperature.ctof(temp)
  end

  def in_celsius
    type == :c ? temp : Temperature.ftoc(temp)
  end

  def self.ctof(temp)
    (temp * 9.0 / 5.0) + 32
  end

  def self.ftoc(temp)
    (temp - 32) * 5.0 / 9.0
  end

  def self.from_celsius(temp)
    Temperature.new(c: temp)
  end

  def self.from_fahrenheit(temp)
    Temperature.new(f: temp)
  end

end

class Celsius < Temperature
  def initialize(temp)
    super(c: temp)
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    super(f: temp)
  end
end
