class Book
  attr_reader :title

  def title=(string)
    articles = ["the", "a", "an", "and", "in", "of"]

    capitalized_words = string.split.map do |word|
      articles.include?(word) ? word : word.capitalize
    end

    capitalized_words.first.capitalize!
    @title = capitalized_words.join(" ")
  end
end
